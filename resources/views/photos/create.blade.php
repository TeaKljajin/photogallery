@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-2">
            <a href="{{route('albumsIndex', $album->id)}}" class="btn btn-primary" style="margin-left:10%;"role="button">Go Back</a>
        </div>
        <div class="col-lg-10">
            <h1 class="cr1">Create Photo</h1>
        </div>
    </div>



    <div class="cr2">
        {!! Form::open(['method'=>'POST','action'=>'PhotosController@store','files'=>true]) !!}
        {!! csrf_field() !!}
        {!! Form::hidden('album_id',$album->id) !!}
        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Photo Description:') !!}
            {!! Form::textarea('description', null, ['class'=>'form-control','rows'=>7]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('photo', 'Photo:') !!}
            {!! Form::file('photo', null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Create Photo', ['class'=>'form-control']) !!}
        </div>


        {!! Form::close() !!}
    </div>
    <div class="container">
            @if(count($errors) > 0)
                @foreach($errors->all() as $error)
                <div class="row btn btn-danger m-2">
                        {{$error}}
                    </div>

                @endforeach
            @endif
    </div>

@endsection

