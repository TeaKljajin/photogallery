@extends('layouts.app')
@section('content')

    <br>
    <a href="{{route('albumsShow', $photo->album->id)}}" class="btn btn-primary m-1" role="button">Back To Gallery</a>
    <hr>
    <div class="row">
        <div class="col-lg-7">
            <img style="margin:10px;width:600px;height:400px;" src="/storage/photos/{{$photo->photo}}" alt="{{$photo->title}}">
        </div>
        <div class="col-lg-5">
            <h3>{{$photo->title}}</h3>
            <p>{{$photo->description}}</p>
            <small>Photo Size: {{$photo->size}}</small>
        </div>
    </div>


    {!! Form::open(['action'=>['PhotosController@destroy', $photo->id],'method'=>'POST']) !!}
    <div class="form-group">
        {{ Form::submit('Delete Photo', ['class'=>'btn btn-danger m-2', 'onclick' => 'return confirm("Are you sure you wont to delete?")']) }}
    </div>

    {!! Form::close() !!}
    <hr>

@endsection


