@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-6" style="margin-top:8%;">
            <h1>Welcome to Photo Gallery</h1>
            <p style="font-size: 18px;text-align:justify">On this website you can create your own album and
                put some picture inside it or you can create or delete picture in alredy existing albums.
                For creating album you must be logged in.Also you can make some description for
                your picture and see how that picture look alone. You have option to delete picture if
                you not like how it looks in album.When you logg in you will see already some albums.</p>

        </div>
        <div class="col-lg-6">
            <img style="margin:10px;width:500px;height:400px;" src="/storage/front-end/horse.jpg">
        </div>
    </div>
    <div class="row">

        <div class="col-lg-6">
            <img style="margin:10px;width:500px;height:400px;" src="/storage/front-end/Dog.jpg">
        </div>
        <div class="col-lg-6" style="margin-top:8%;">
            <h1>Albums</h1>
            <p style="font-size: 18px;text-align:justify">When you choosing cover_image for your
            album please try to select picture witch will corenspodense with the rest pictures in your albums.
                For example if you won't to make album about horses, than do not put cover_image which doesn't
                have anything with horses.</p>


        </div>
    </div>
    <div class="row">
        <div class="col-lg-6" style="margin-top:8%;">
            <h1>Photos</h1>
            <p style="font-size: 18px;text-align: justify">On this website you can create as mush as
                you won't pictures inside one album. Just make sure that the size of the picture you are
                uploading is less than 1999 or you will not be  able to upload it. There is many free websites
                to download free pictures. Use your imagination and be creative while making your album.</p>


        </div>
        <div class="col-lg-6">
            <img style="margin:10px;width:500px;height:400px;" src="/storage/front-end/Pelican.jpg">
        </div>
    </div>
</div>




@endsection

