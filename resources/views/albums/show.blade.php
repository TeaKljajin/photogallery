@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-2" >
            <a href="{{route('photoCreate', $album->id)}}" class="btn btn-primary" style="margin-left:7%;"role="button">Create New Photo</a>

        </div>
        <div class="col-lg-8">
            <h1 style="margin-left: 21%;">All Photos by Album {{$album->name}}</h1>

            <p style="margin-left: 21%;font-size: 18px;">Click on picture to see the full description or make new one</p>
        </div>
        <div class="col-lg-2">
            <a style="margin-left: 45%;" href="{{route('albumsIndex')}}" class="btn btn-primary" style="margin-left:10%;"role="button">Go Back</a>
        </div>
    </div>
    <div class="container" style="margin-left:6%;">
    @if(count($album->photos) > 0)
    <?php
            $colcount = count($album->photos);
            $i = 1;
      ?>
    <div id="photos">
        <div class="row text-center">
            @foreach($album->photos as $photo)
            @if($i == $colcount)
            <div class="medium-4 columns end">
                <a href="{{route('photoShow', $photo->id)}}">
                    <img class="thumbnail" style="margin:10px;width:300px;height:300px;" src="/storage/photos/{{$photo->photo}}" alt="{{$photo->title}}">
                </a>
                    <br>
                <h4>   <a href="{{route('photoShow', $photo->id)}}">{{$photo->title}}</a></h4>
                    @else
                        <div class="medium-4 columns">
                            <a href="{{route('photoShow', $photo->id)}}">
                                <img class="thumbnail" style="margin:10px;width:300px;height:300px;" src="/storage/photos/{{$photo->photo}}" alt="{{{$photo->title}}}">
                            </a>
                                <br>
                            <h4>   <a href="{{route('photoShow', $photo->id)}}">{{$photo->title}}</a></h4>
                                @endif
                                @if($i % 3 == 0)
                        </div></div><div class="row text-center">
                @else
            </div>
            @endif
            <?php $i++ ?>
            @endforeach
        </div>
    </div>
    @else
        <p style="margin-bottom: 200px;margin-top:200px;margin-left:34%;font-size: 18px">No Photos To Display</p>
    @endif
    </div>

@endsection


