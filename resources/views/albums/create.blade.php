@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-2">
            <a href="{{route('albumsIndex')}}" class="btn btn-primary" style="margin-left:10%;"role="button">Go Back</a>
        </div>
        <div class="col-lg-10">
            <h1 class="cr1">Create Album</h1>
        </div>
    </div>



    <div class="cr2">
    {!! Form::open(['method'=>'POST','action'=>'AlbumsController@store','files'=>true]) !!}
    {!! csrf_field() !!}
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('description', 'Description:') !!}
        {!! Form::textarea('description', null, ['class'=>'form-control','rows'=>7]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('cover_image', 'Cover image:') !!}
        {!! Form::file('cover_image', null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Create Album', ['class'=>'form-control']) !!}
    </div>


    {!! Form::close() !!}
    </div>

    <div class="container">
        @if(count($errors) > 0)
            @foreach($errors->all() as $error)
                <div class="row btn btn-danger m-2">
                    {{$error}}
                </div>

            @endforeach
        @endif
    </div>
@endsection
