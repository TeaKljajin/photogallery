@extends('layouts.app')
@section('content')

        @if(session('success'))
            <div class="alert alert-success col-lg-12 m-2">
                {{session('success')}}
            </div>
        @endif


    <div class="row">
        <div class="col-lg-2" >
            <a href="{{route('albumsCreate')}}" class="btn btn-primary" style="margin-left:6%;"role="button">Create New Album</a>

        </div>
        <div class="col-lg-10">
            <h1 style="margin-left: 25%;">All Albums</h1>
        </div>
    </div>

    <div class="container" >
    @if(count($albums) > 0)
    <?php
            $colcount = count($albums);
            $i = 1;
      ?>
    <div id="albums">
        <div class="row text-center">
            @foreach($albums as $album)
            @if($i == $colcount)
            <div class="medium-4 columns end">
                <a href="{{route('albumsShow', $album->id)}}">
                    <img style="margin:10px;width:200px;height:200px;"  class="thumbnail" src="storage/album_covers/{{$album->cover_image}}">
                </a>
                    <br>
                    <h4><a href="{{route('albumsShow', $album->id)}}">{{$album->name}}</a></h4>
                @else
                    <div class="medium-4 columns">
                        <a href="{{route('albumsShow', $album->id)}}">
                                <img style="margin:10px;width:200px;height:200px;"  class="thumbnail" src="storage/album_covers/{{$album->cover_image}}">
                        </a>
                        <br>
                        <h4><a href="{{route('albumsShow', $album->id)}}">{{$album->name}}</a></h4>
                        @endif
                        @if($i % 5 == 0)
                        </div></div><div class="row text-center">
                @else
            </div>
            @endif
            <?php $i++ ?>
            @endforeach
        </div>
    </div>
    @else
        <p>No Albums To Display</p>
    @endif
    </div>
@endsection

