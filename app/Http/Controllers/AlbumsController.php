<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Album;
use App\Photo;

class AlbumsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $albums = Album::all();
        return view('albums.index', compact('albums'));
    }

    public function create(){
        return view('albums.create');
    }

    public function store(Request $request){
       $this->validate($request,[
           'name'=>'required',
           'description'=>'required',
           'cover_image'=>'required'
       ]);

       //Get filename with extension
        $filenameWithExt= $request->file('cover_image')->getClientOriginalName();
        //Get just the filename
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        //Get the extension
        $extension = $request->file('cover_image')->getClientOriginalExtension();
        //Create new filename
        $filenameToStore = $filename.'_' .time().'.' .$extension;
        //Upload Image
        $path = $request->file('cover_image')->storeAs('public/album_covers',$filenameToStore);

        //Create Album
        $album = new Album;
        $album->name = $request->input('name');
        $album->description = $request->input('description');
        $album->cover_image = $filenameToStore;

        $album->save();
        return redirect('/albums')->with('success', 'Album has been created successfully');


    }

    public function show($id){
           $album = Album::with('photos')->find($id);


           return view('albums.show')->with('album', $album);
    }





}
