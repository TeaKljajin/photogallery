<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Photo;
use App\Album;

class PhotosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create($id){
        $album = Album::findOrFail($id);
        return view('photos.create',compact('album'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'title'=>'required',
            'description'=>'required',
            'photo'=>'required'
        ]);

        //Get filename with extension
        $filenameWithExt= $request->file('photo')->getClientOriginalName();
        //Get just the filename
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        //Get the extension
        $extension = $request->file('photo')->getClientOriginalExtension();
        //Create new filename
        $filenameToStore = $filename.'_' .time().'.' .$extension;
        //Upload Image
        $path = $request->file('photo')->storeAs("public/photos",$filenameToStore);





        //Create Album
        $photo = new Photo;
        $photo->title = $request->input('title');
        $photo->description = $request->input('description');
        $photo->photo = $filenameToStore;
        $photo->album_id = $request->input('album_id');
        $photo->size = $request->file('photo')->getSize();

        $photo->save();
        return redirect('/albums')->with('success', 'Photo has been created successfully');


    }

    public function show($id){
       $photo = Photo::findOrFail($id);
       return view('photos.show', compact('photo'));

    }

    public function destroy($id){
        $photo = Photo::findOrFail($id);
        if(file_exists($photo->photo)){
            unlink(public_path()."public/photos/$photo->photo");
        }
        $photo->delete();
       return redirect('/albums')->with('success','Photo has been deleted successfully');
    }

}
