<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AlbumsController@index')->name('albumsIndex');
Route::get('/albums', 'AlbumsController@index')->name('albumsIndex');
Route::get('/albums/create','AlbumsController@create')->name('albumsCreate');
Route::post('/albums/store','AlbumsController@store')->name('albumsStore');
Route::get('/albums/{id}/photos', 'AlbumsController@show')->name('albumsShow');

Route::get('/{id}/photo/create', 'PhotosController@create')->name('photoCreate');
Route::post('/photo/store', 'PhotosController@store')->name('photoStore');
Route::get('/photo/{id}/show', 'PhotosController@show')->name('photoShow');
Route::post('/photo/{id}/delete', 'PhotosController@destroy')->name('photoDestroy');

Auth::routes();

Route::get('/', 'PublicsController@index')->name('publicsIndex');

Auth::routes();


