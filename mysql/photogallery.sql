-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 24, 2020 at 01:05 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `photogallery`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `name`, `description`, `cover_image`, `created_at`, `updated_at`) VALUES
(15, 'Horses', 'Album with horses picture', 'horse-197199_1280_1590316645.jpg', '2020-05-24 08:37:26', '2020-05-24 08:37:26'),
(16, 'Dogs', 'Album with dogs picture', 'dog-3139757_1280_1590316687.jpg', '2020-05-24 08:38:07', '2020-05-24 08:38:07'),
(17, 'Birds', 'Album with birds picture', 'owl-1576572_1280_1590316730.jpg', '2020-05-24 08:38:50', '2020-05-24 08:38:50'),
(18, 'Leo', 'Leo is the fifth astrological sign of the zodiac. It corresponds to the constellation', 'lion-1211424_640_1590318006.jpg', '2020-05-24 09:00:06', '2020-05-24 09:00:06'),
(19, 'Elephant', 'Elephants are mammals of the family Elephantidae and the largest existing land animals', 'elephant-111695_640_1590318177.jpg', '2020-05-24 09:02:57', '2020-05-24 09:02:57'),
(20, 'Pinquin', 'Penguins are a group of aquatic flightless birds.', 'pinquin-2304495_640_1590318270.jpg', '2020-05-24 09:04:31', '2020-05-24 09:04:31');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_05_21_165605_create_albums_table', 1),
(5, '2020_05_21_165649_create_photos_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `album_id` int(11) NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `album_id`, `photo`, `title`, `size`, `description`, `created_at`, `updated_at`) VALUES
(27, 15, 'horses-1984977_1280_1590316952.jpg', 'Horse 1', '293445', 'The horse (Equus ferus caballus) is one of two extant subspecies of Equus ferus. It is an odd-toed ungulate mammal belonging to the taxonomic family Equidae.', '2020-05-24 08:42:32', '2020-05-24 08:42:32'),
(28, 15, 'animal-3099035_1280_1590317025.jpg', 'Horse 2', '300291', 'The horse has evolved over the past 45 to 55 million years from a small multi-toed creature, Eohippus, into the large, single-toed animal of today.', '2020-05-24 08:43:45', '2020-05-24 08:43:45'),
(29, 15, 'iceland-horses-4649468_1280_1590317067.jpg', 'Horse 3', '197641', 'Horses still hold a place of honor in many cultures, often linked to heroic exploits in war. Wild and Domesticated.', '2020-05-24 08:44:27', '2020-05-24 08:44:27'),
(30, 15, 'horses-2427513_1280_1590317113.jpg', 'Horse 4', '351085', 'Horse, a hoofed herbivorous mammal of the family Equidae. It comprises a single species, Equus caballus, whose numerous varieties are called breeds.', '2020-05-24 08:45:13', '2020-05-24 08:45:13'),
(31, 16, 'dogs-4477058_1280_1590317196.jpg', 'Dog 1', '260162', 'The dog (Canis familiaris when considered a distinct species or Canis lupus familiaris when considered a subspecies of the wolf) is a member of the genus Canis (canines).', '2020-05-24 08:46:36', '2020-05-24 08:46:36'),
(32, 16, 'australian-shepherd-3237735_1280_1590317238.jpg', 'Dog 2', '227994', 'The dog is a member of the genus Canis, which forms part of the wolf-like canids, and is the most widely abundant terrestrial carnivore.', '2020-05-24 08:47:18', '2020-05-24 08:47:18'),
(33, 16, 'dog-2350955_1280_1590317281.jpg', 'Dog 3', '436506', 'Dogs Trust is the UK\'s largest Dog Welfare Charity. Looking to rehome a rescue dog or to donate to an animal charity? Visit us today to find out more!', '2020-05-24 08:48:01', '2020-05-24 08:48:01'),
(35, 16, 'nature-3047194_1280_1590317376.jpg', 'Dog 4', '229181', 'Today humans have bred hundreds of different domestic dog breeds—some of which could never survive.', '2020-05-24 08:49:36', '2020-05-24 08:49:36'),
(36, 17, 'wild-goose-2260866_1280_1590317444.jpg', 'Bird 1', '321839', 'Birds are vertebrate animals adapted for flight. Many can also run, jump, swim, and dive. Some, like penguins, have lost the ability to fly but retained their wings.', '2020-05-24 08:50:44', '2020-05-24 08:50:44'),
(37, 17, 'owl-2903707_1280_1590317475.jpg', 'Bird 2', '238971', 'Birds are found worldwide and in all habitats. The largest is the nine-foot-tall ostrich.', '2020-05-24 08:51:15', '2020-05-24 08:51:15'),
(38, 17, 'kingfisher-881975_1280_1590317603.jpg', 'Bird 3', '110256', 'Many species of birds are economically important as food for human consumption and raw material in manufacturing,', '2020-05-24 08:53:23', '2020-05-24 08:53:23'),
(39, 17, 'gulls-654046_1280_1590317722.jpg', 'Bird 4', '148125', 'Birds live and breed in most terrestrial habitats and on all seven continents, reaching their southern extreme.', '2020-05-24 08:55:22', '2020-05-24 08:55:22'),
(40, 18, 'lion-1840092_640_1590318056.jpg', 'Leo 1', '96111', 'People born under the sign of Leo are natural born leaders.', '2020-05-24 09:00:56', '2020-05-24 09:00:56');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Tea', 'tea@gmail.com', NULL, '$2y$10$XZ49Z8jZoWRBSMOhSYSMP.AutSy5t/B/Xz7MyK8ktBQO3xYl561L2', NULL, '2020-05-21 21:04:17', '2020-05-21 21:04:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
